# SMPL-X Blender Add-on

This add-on allows you to add [SMPL-X](https://smpl-x.is.tue.mpg.de) skinned meshes to your current Blender scene. Each imported SMPL-X mesh consists of a shape specific rig, as well as shape keys (blend shapes) for shape, expression and pose correctives.

+ Requirements: Blender 3.6+, tested with 4.2.3
+ Additional dependencies: None
+ Used SMPL-X models:
  + SMPL-X locked head (no head bun), this is the new default model
  + SMPL-X v1.1
  + 300 shape components, 100 expression components

# Features
+ Add female/male/neutral specific SMPL-X mesh to current scene
+ Set sample albedo texture
+ Set body shape from height and weight measurements
+ Randomize/reset shape
+ Update joint locations
+ Position feet on ground plane (z=0)
+ Randomize/reset face expression shape
+ Enable/disable corrective poseshapes
+ Change hand pose (flat, relaxed)
+ Write current pose in SMPL-X theta notation to console
+ Load pose from .pkl file
    + Format: Full body pose with 55 joints in Rodrigues notation
    + Over 3000 sample poses are available at https://agora.is.tuebingen.mpg.de/
        + Sign In > Download > Ground Truth Fittings > SMPL-X fits
+ Create animated body from animation .npz file (AMASS or SMPL-X)
    + Use "Locked Head" model version when working with animation files from AMASS
+ Alembic (.abc) export of animated body as animated vertex geometry cache
    + Keyframed pose correctives are always baked into the vertices on export
    + Alembic animation can be imported into other third-party tools
        + Unreal Engine
            + Alembic Import settings: Geometry Cache, Scale (100, -100, 100), Rotation (90, 0, 0)
    + We recommend latest Blender 3 release for up-to-date Alembic format support
+ FBX export
    + Export to Unity or Unreal Engine
        + Imported FBX will import in Unity/Unreal without rotations and without scaling
    + Shape key export options: 
        + Body shape and pose correctives
        + Body shape without pose correctives
        + None (bakes current body shape into mesh, removes all pose correctives)
        + Pose correctives only (bakes current body shape and expression into mesh, keeps all shape keys for pose correctives)

## Installation
1. Register at https://smpl-x.is.tue.mpg.de and download the SMPL-X for Blender add-on. The ZIP release file will include the required SMPL-X model which is not included in the code repository.
2. Blender>Edit>Preferences>Add-ons>Install
3. Select downloaded SMPL-X for Blender add-on ZIP file (`smplx_blender_addon-YYYYMMDD.zip`) and install
4. Enable SMPL-X for Blender add-on
5. Enable sidebar in 3D Viewport>View>Sidebar
6. SMPL-X tool will show up in sidebar

## Usage
+ [Short overview video](https://www.youtube.com/watch?v=DY2k29Jef94)
+ [CVPR 2021 tutorial video](https://www.youtube.com/watch?v=m8i00zG6mZI&t=107s)

## Notes
+ The add-on GUI (gender, texture, hand pose) does not reflect the state of the currently selected SMPL-X model if you work with multiple models in one scene.
+ To maintain editor responsiveness the add-on does not automatically recalculate joint locations when you change the shape manually via Blender shape keys. Use the `Update Joint Locations` button to update the joint locations after manual shape key change.
+ To maintain editor responsiveness, the add-on does not automatically recalculate the corrective pose shape keys when you change the armature pose. Use the `Update Pose Shapes` button to update the joint locations after pose changes.
+ Setting shape from height and weight should be used with the v1.1 model for best results
+ This add-on supports both `locked head (no head bun)` and `v1.1` model versions. Make sure to select the correct one before using the AddAnimation option.
  + Use `locked head` when working with AMASS animations

## License
+ Generated body mesh data using this add-on:
    + Licensed under SMPL-X Model or Body License, depending on use case
        + https://smpl-x.is.tue.mpg.de/modellicense.html
        + https://smpl-x.is.tue.mpg.de/bodylicense.html

+ See LICENSE.md for further license information including commercial licensing

+ Attribution for publications: 
    + You agree to cite the most recent paper describing the model as specified on the SMPL-X website: https://smpl-x.is.tue.mpg.de

## Acknowledgements
+ We thank [Meshcapade](https://meshcapade.com/) for providing the SMPL-X female/male sample textures under [Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/) license.

+ Sergey Prokudin (rainbow texture data)

+ Vassilis Choutas (betas-to-joints regressor)

+ Lea Müller and Vassilis Choutas (measurements-to-betas regressor)

## Changelog
+ 20210505: Initial release
+ 20210525: Replace vertices-to-joints regressor with beta-to-joints regressor. Add rainbow texture (CC BY-NC 4.0).
+ 20210611: Add option to set shape from height and weight values for female and male models
+ 20210629: Add option to create animated body from AMASS SMPL-X animation file
+ 20220117: Add option to set height+weight for neutral SMPL-X model
+ 20220218: Add option to set animation target framerate. Lower values will speed up import time.
+ 20220311:
  + Fix pelvis location offset when importing AMASS animations
  + Add option to set animation import format (AMASS, SMPL-X)
  + Adjust Blender Timeline end frame when adding first animation
  + Use 30fps as new default target framerate
  + Add Alembic export button
+ 20220315:
  + Speed up animation import time
+ 20220326:
  + Add Unreal FBX export. Shape keys bake options can now be found in export dialog settings.
  + Fix unwanted duplicated animation sequence in FBX export
+ 20220623:
  + Add option to import animation onto grounded rest pose armature
  + Disable animation keyframe simplification for FBX export so that FBX animations match Alembic animations
  + Add support for 300 beta shape model
+ 20230120:
  + Add option to use relaxed hand reference frame when adding animation from file
+ 20230302:
  + Add SMPL-X locked head (no head bun) model and option to choose between v1.1 and locked head
    + Setting shape from height and weight is only correct for v1.1 model
  + The locked head (no head bun) model is the new default option
    + Use this model for AMASS animations
    + Python scripts which want to use v1.1 model need to update the code to select v1.1 model
  + Use models with 100 expressions
  + Use custom properties on mesh object to store version and gender for internal processing instead of depending on proper object name tags
+ 20240206:
  + Add support for UV map 2023 version and corresponding female/male sample textures
  + UV map 2023 is new default
+ 20240408:
  + Add option to export shape values (and optional bind pose height offset) to .npz
    + If animation was imported with grounded bind pose the applied height offset change is stored as object property so that it can later be exported in the shape-only npz as `bind_pose_height_offset` key. This is needed to be able to later remap grounded bind pose animations to standard bind pose animations.
      + If body shape was created via shape key modification then the offset result from SnapToGroundPlane is stored. Armature must be in default pose for correct SnapToGroundPlane height offset calculation.
+ 20240418:
  + Fix rainbow texture dark areas at vertices 4146 and 6553
+ 20241129:
  + Save custom properties when using "Export FBX" so that add-on can also be used for reimported SMPL-X FBX files
  + Ensure valid shape key slider ranges so that add-on can also be used for reimported SMPL-X FBX files where initial range is [0, 1]
  + Add new blend shape export option to FBX export for exporting only pose corrective blend shapes
    + bakes current body shape and expression into mesh, keeps all pose correctives

## Contact
+ smplx-blender@tue.mpg.de
